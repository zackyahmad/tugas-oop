<?php
    require_once('Animal.php');
    require_once('Ape.php');
    require_once('Frog.php');
    $animal = new Animal("Shaun");

    echo "Nama: ".$animal->name ."<br>";
    echo "legs: ".$animal->legs ."<br>";
    echo "cold blooded: ". $animal->cold_blooded."<br>"."<br>";

    $frog =  new Frog("Buduk");
    echo "Nama: ".$frog->name ."<br>";
    echo "legs: ".$frog->legs ."<br>";
    echo "cold blooded: ". $frog->cold_blooded."<br>";
    echo "jump : ".$frog->jump("Hop Hop"). "<br><br>";

    $ape = new Ape("Kera Sakti");
    echo "Nama: ".$ape->name ."<br>";
    echo "legs: ".$ape->legs=2 ."<br>";
    echo "cold blooded: ". $ape->cold_blooded."<br>";
    echo "Yell: ".$ape->yell("Auooo") ."<br>"."<br>";



?>